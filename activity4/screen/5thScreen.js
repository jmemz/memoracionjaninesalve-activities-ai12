import React from "react";
import {View, TouchableOpacity, Text, StyleSheet, Image } from 'react-native'

const FifthScreen = ({navigation}) => {
    return (
        <View style = {styles.container}>
            <Image source={require ('../images/undraw_Mobile_development_re_wwsn.png')}
                style={{width:380,height:280}}/>
            <Text style = {styles.text}> Learning developments {'\n'} in technology and {'\n'} be able to use it to {'\n'} its full potential </Text>
            <TouchableOpacity onPress= { () => navigation.navigate('Home')}>
                <View style={styles.press}>
                <Text style={styles.pressButton}>Home</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: '#a8dadc',
        justifyContent: 'center', 
        alignItems: 'center',
    },
    text: {
        fontSize: 25,
        marginTop: 30,
        fontWeight: 'bold',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        color: '#457b9d'
    },
    press: {
        width: 200,
        height: 40 ,
        marginTop: 50,
        marginBottom: 10,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f1faee'
    },
    pressButton: {
        color: '#1d3557',
        fontWeight: 'bold',
        fontSize: 19
    }
});
export default FifthScreen;