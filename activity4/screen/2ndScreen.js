import React from "react";
import {View, TouchableOpacity, Text, StyleSheet, Image } from 'react-native'


const SecondScreen = ({navigation}) => {
    return (
        <View style = {styles.container}>
            <Image source={require ('../images/undraw_online_resume_re_ru7s.png')}
                style={{width:390,height:250, left: 6}}/>
            <Text style = {styles.text}> Numerous job {'\n'} opportunities {'\n'} after college </Text>
            <TouchableOpacity onPress= { () => navigation.navigate('3rdScreen')}>
                <View style={styles.press}>
                <Text style={styles.pressButton}>Continue</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: '#457b9d',
        justifyContent: 'center', 
        alignItems: 'center',
    },
    text: {
        fontSize: 25,
        marginTop: 25,
        fontWeight: 'bold',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        color: '#e0fbfc'
    },
    press: {
        width: 200,
        height: 40 ,
        marginTop: 60,
        marginBottom: 30,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#e07a5f'
    },
    pressButton: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 19
    }
});

export default SecondScreen;