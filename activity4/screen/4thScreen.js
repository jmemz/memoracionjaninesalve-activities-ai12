import React from "react";
import {View, TouchableOpacity, Text, StyleSheet, Image } from 'react-native'

const FourthScreen = ({navigation}) => {
    return (
        <View style = {styles.container}>
            <Image source={require ('../images/undraw_Revenue_re_2bmg.png')}
                style={{width:250,height:300}}/>
            <Text style = {styles.text}> IT related jobs have {'\n'} competitive salaries </Text>
            <TouchableOpacity onPress= { () => navigation.navigate('5thScreen')}>
                <View style={styles.press}>
                <Text style={styles.pressButton}>Continue</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: '#1d3557',
        justifyContent: 'center', 
        alignItems: 'center',
    },
    text: {
        fontSize: 25,
        marginTop: 40,
        fontWeight: 'bold',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        color: '#fff'
    },
    press: {
        width: 200,
        height: 40 ,
        marginTop: 60,
        marginBottom: 10,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#a8dadc'
    },
    pressButton: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 19
    }
});

export default FourthScreen;