import React from "react";
import {View, TouchableOpacity, Text, StyleSheet, Image } from 'react-native'

const HomeScreen = ({navigation}) => {
    return (
        <View style = {styles.container}>
            <Image source={require ('../images/undraw_starlink_3r0a.png')}
                style={{width:400,height:300, left: 4}}/>
            <Text style = {styles.text}> Why did  {'\n'} I chose IT? </Text>
            <TouchableOpacity onPress= { () => navigation.navigate('2ndScreen')}>
                <View style={styles.press}>
                <Text style={styles.pressButton}>Continue</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: '#f1faee',
        justifyContent: 'center', 
        alignItems: 'center',
    },
    text: {
        fontSize: 40,
        fontWeight: 'bold',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        color: '#3d5a80'
    },
    press: {
        width: 200,
        height: 40 ,
        marginTop: 50,
        marginBottom: 30,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#457b9d'
    },
    pressButton: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 19
    }
});

export default HomeScreen;