import React from "react";
import {View, TouchableOpacity, Text, StyleSheet, Image} from 'react-native'

const ThirdScreen = ({navigation}) => {
    return (
        <View style = {styles.container}>
            <Image source={require ('../images/undraw_Percentages_re_a1ao.png')}
                style={{width:380,height:250}}/>
            <Text style = {styles.text}> Low cost {'\n'} of education </Text>
            <TouchableOpacity onPress= { () => navigation.navigate('4thScreen')}>
                <View style={styles.press}>
                <Text style={styles.pressButton}>Continue</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: '#e07a5f',
        justifyContent: 'center', 
        alignItems: 'center',
    },
    text: {
        fontSize: 25,
        marginTop: 30,
        fontWeight: 'bold',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        color: '#fff'
    },
    press: {
        width: 200,
        height: 40 ,
        marginTop: 60,
        marginBottom: 10,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#1d3557'
    },
    pressButton: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 19
    }
});

export default ThirdScreen;