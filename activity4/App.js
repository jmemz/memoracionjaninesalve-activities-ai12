import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { StyleSheet, Text, View } from 'react-native';
import HomeScreen from "./screen/1stScreen.js";
import SecondScreen from "./screen/2ndScreen.js";
import ThirdScreen from "./screen/3rdScreen.js";
import FourthScreen from "./screen/4thScreen.js";
import FifthScreen from "./screen/5thScreen.js";


export default function App() {
  const Stack = createNativeStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name = "Home" component={HomeScreen} />
        <Stack.Screen name = "2ndScreen" component={SecondScreen} />
        <Stack.Screen name = "3rdScreen" component={ThirdScreen} />
        <Stack.Screen name = "4thScreen" component={FourthScreen} />
        <Stack.Screen name = "5thScreen" component={FifthScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}


