import React, {useState} from 'react';
import { StyleSheet, Text, View,TextInput } from 'react-native';
import { TouchableOpacity, Dimensions } from 'react-native';
import { ScrollView } from 'react-native';
import List from './components/TaskList';

export default function App() {
  const [text,setText] =useState();
  const [taskList,setTaskList] = useState ([]);
  const handlePress = () => {
    setTaskList([ text,...taskList])
    setText (null);
  }
  const done = (index) => {
    var items = [...taskList];
    items.splice(index,1);
    setTaskList(items);
  }
  return (
    <View style={styles.container}>
      <View style= {styles.topTexts}>
      <TextInput 
        style={styles.input}
        placeholder = {'Input a Task'}
        value = {text}
        onChangeText = {text =>setText(text) }
        />
      <TouchableOpacity onPress={() => handlePress()}>
        <View style={styles.press}>
          <Text style={styles.pressButton}>+</Text>
        </View>
      </TouchableOpacity>
      </View>
      <View style = {styles.title}>
        <Text style= {styles.titleDesign}>YOUR TASKS</Text>
        <ScrollView>
          <View> 
            {
              taskList.map((item, index) => {
                return (
                <TouchableOpacity 
                key={index}  
                onPress={() => done(index)}
                style = {styles.delete}>
                  <List key={index} text= {item}/>
                  <Text style={styles.check}> TASK DONE ✓ </Text>
                </TouchableOpacity>
              ) 
              })
            }
          </View>
        </ScrollView>
      </View>
    </View>
  );
}
const {width, height} = Dimensions.get("screen");
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fdfcdc',
  },
  title: {
    paddingTop: 170,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleDesign: {
    marginTop: 5,
    fontSize: 20,
    fontWeight:'bold',
  },
  topTexts: {
    position: 'absolute',
    top: 20,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  input: {
    padding: 5,
    marginTop: 80,
    marginLeft: 23,
    width: 250,
    borderWidth: 2,
    borderRadius: 30,
    borderColor:'black',
    borderStyle: 'dashed',
    textAlign: 'center',
  },
  press: {
    width: 45,
    height: 45,
    marginTop: 80,
    marginRight:23,
    borderWidth: 2,
    borderRadius: 30,
    borderColor:'black',
    borderStyle: 'dashed',
    alignItems: 'center',
    justifyContent: 'center',
  },
  delete: {
    backgroundColor: '#e9ecef',
    marginTop: 18,
    width: width/1.1,
    borderRadius: 10,
    alignItems: 'center',
    borderStyle: 'solid',
    borderWidth: 2,
  },
  check: {
    textAlign: 'center',
    marginTop: 7,
    marginBottom: 12,
    fontWeight: 'bold',
    color: 'green',
  }
});
