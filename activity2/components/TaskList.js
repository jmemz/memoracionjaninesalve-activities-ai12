import React from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';

const List = (props) => {
    return (
        <View style={styles.task}>
            <Text style= {styles.taskDesign}>{props.text}</Text>
        </View>
    )
}
const {width, height} = Dimensions.get("screen");
const styles = StyleSheet.create ({
    task: {
    backgroundColor: '#fff',
    padding: 15,
    margin: 15,
    width: width/1.2,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
    color: 'black',
    justifyContent: 'center',
    marginBottom: 5,
    },
    taskDesign: {
        maxWidth: '80%',
        fontSize: 18,
        
    }
});
export default List;