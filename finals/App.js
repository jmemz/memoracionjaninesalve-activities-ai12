import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from "./screens/HomeScreen";
import StudioPreviewScreen from "./screens/StudioPreviewScreen";
import FirstStudio from "./screens/1stStudio/1stStudio";
import AFirstPackage from "./screens/1stStudio/Packages/A1";
import ASecondPackage from "./screens/1stStudio/Packages/A2";
import AThirdPackage from "./screens/1stStudio/Packages/A3";
import SecondStudio from "./screens/2ndStudio/2ndStudio";
import BFirstPackage from "./screens/2ndStudio/packages/B1";
import BSecondPackage from "./screens/2ndStudio/packages/B2";
import BThirdPackage from "./screens/2ndStudio/packages/B3";
import ThirdStudio from "./screens/3rdStudio/3rdStudio";
import CFirstPackage from "./screens/3rdStudio/Packages/C1";
import CSecondPackage from "./screens/3rdStudio/Packages/C2";
import CThirdPackage from "./screens/3rdStudio/Packages/C3";
import FourthStudio from "./screens/4thStudio/4thStudio";
import DFirstPackage from "./screens/4thStudio/Packages/D1";
import DSecondPackage from "./screens/4thStudio/Packages/D2";
import DThirdPackage from "./screens/4thStudio/Packages/D3";
import FifthStudio from "./screens/5thStudio/5thStudio";
import EFirstPackage from "./screens/5thStudio/Packages/E1";
import ESecondPackage from "./screens/5thStudio/Packages/E2";
import EThirdPackage from "./screens/5thStudio/Packages/E3";

export default function App() {
 const Stack = createNativeStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name = "Home" component={HomeScreen} />
        <Stack.Screen name = "StudioPreviewScreen" component={StudioPreviewScreen} />
        <Stack.Screen name = "1stStudio" component={FirstStudio}/>
          <Stack.Screen name = "A1" component={AFirstPackage}/>
          <Stack.Screen name = "A2" component={ASecondPackage}/>
          <Stack.Screen name = "A3" component={AThirdPackage}/>
        <Stack.Screen name = "2ndStudio" component={SecondStudio}/>
          <Stack.Screen name = "B1" component={BFirstPackage}/>
          <Stack.Screen name = "B2" component={BSecondPackage}/>
          <Stack.Screen name = "B3" component={BThirdPackage}/>
        <Stack.Screen name = "3rdStudio" component={ThirdStudio}/>
          <Stack.Screen name = "C1" component={CFirstPackage}/>
          <Stack.Screen name = "C2" component={CSecondPackage}/>
          <Stack.Screen name = "C3" component={CThirdPackage}/>
        <Stack.Screen name = "4thStudio" component={FourthStudio}/>
         <Stack.Screen name = "D1" component={DFirstPackage}/>
         <Stack.Screen name = "D2" component={DSecondPackage}/>
         <Stack.Screen name = "D3" component={DThirdPackage}/>
        <Stack.Screen name = "5thStudio" component={FifthStudio}/>
          <Stack.Screen name = "E1" component={EFirstPackage}/>
          <Stack.Screen name = "E2" component={ESecondPackage}/>
          <Stack.Screen name = "E3" component={EThirdPackage}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

  