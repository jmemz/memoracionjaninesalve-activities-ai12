import React from "react";
import {View, TouchableOpacity, Text, StyleSheet, Image, ScrollView, Dimensions} from 'react-native'


const StudioPreviewScreen = ({navigation}) => {
    return (
    <View style = {styles.container}> 
    <ScrollView> 
        <Image source={require ('../images/logo.png')}
            style={{width: 200 ,height:80, marginTop: 30, marginLeft: 80, }}/>
        <View style= {styles.first}>
            <TouchableOpacity onPress= { () => navigation.navigate('1stStudio')}>
                <Image source={require ('../images/mlogo.jpg')}
                    style={{width:80,height:80, marginLeft: 120, borderRadius: 50, top: 18}} />
                <View style={styles.press}>
                <Text style={styles.pressButton}> MEMO CLIPS</Text>
                </View>
            </TouchableOpacity>
        </View>
        <View style= {styles.first}>
            <TouchableOpacity onPress= { () => navigation.navigate('2ndStudio')}>
                <Image source ={require ('../images/opticuslogo.jpg')}
                    style={{width:80,height:80, marginLeft: 120, borderRadius: 50, top: 18}} />
                <View style={styles.press}>
                <Text style={styles.opticus}>OPTICUS MOTION PICTURES</Text>
                </View>
            </TouchableOpacity>
        </View>
        <View style= {styles.first}>
            <TouchableOpacity onPress= { () => navigation.navigate('3rdStudio')}>
                <Image source={require ('../images/timelesslogo.jpg')}
                    style={{width:80,height:80, marginLeft: 120, borderRadius: 50, top: 18}} />
                <View style={styles.press}>
                <Text style={styles.pressButton}>TIMELESS VISION</Text>
                </View>
            </TouchableOpacity>
        </View>
        <View style= {styles.first}>
            <TouchableOpacity onPress= { () => navigation.navigate('4thStudio')}>
                <Image source={require('../images/kdlogo.jpg')}
                    style={{width:80,height:80, marginLeft: 120, borderRadius: 50, top: 18}} />
                <View style={styles.press}>
                <Text style={styles.pressButton}> KD CREATIVES</Text>
                </View>
            </TouchableOpacity>
        </View>
        <View style= {styles.first}>
            <TouchableOpacity onPress= { () => navigation.navigate('5thStudio')}>
                <Image source={require ('../images/litratistangwaraylogo.jpg')}
                    style={{width:80,height:80, marginLeft: 120, borderRadius: 50, top: 18}} />
                <View style={styles.press}>
                <Text style={styles.litratista}> LITRATISTANG WARAY</Text>
                </View>
            </TouchableOpacity>
        </View>
    </ScrollView>
    </View>
    )
}
const {width, height} = Dimensions.get("screen");
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: '#FAF3DD',
    },
    first: {
        backgroundColor: '#1e3d59',
        height: height/4,
        width: width/1.1,
        marginTop: 15,
        borderRadius: 10,
        marginLeft: 15,
        borderColor: '#1e3d59',
        borderWidth: 2,
        alignItems: 'flex-start',

    },
    press: {
        width: 200,
        height: 50,
        marginLeft: 60,
        marginTop: 19,
        alignItems: 'center',
        justifyContent: 'center',

    },
    pressButton: {
        color: 'white',
        fontWeight: 'bold',
        marginBottom: 5,
        fontSize: 20
    },
    opticus: {
        color: 'white',
        fontWeight: 'bold',
        marginBottom: 5,
        fontSize: 15
    },
    litratista: {
        color: 'white',
        fontWeight: 'bold',
        marginBottom: 5,
        fontSize: 18
    }
});

export default StudioPreviewScreen;