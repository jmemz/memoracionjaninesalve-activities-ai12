import React from "react";
import {View, TouchableOpacity, Text, StyleSheet, Image, } from 'react-native'


const HomeScreen = ({navigation}) => {
    return (
        <View style = {styles.container}>
             <Image source={require ('../images/logo.png')}
                style={{width: 200 ,height:80, marginTop: 70, marginLeft: 10}}/>
             <Image source={require ('../images/SeekPng.com_camera-png_32940.png')}
                style={{width: 400 ,height:300, marginTop: 10}}/>
            <Text style = {styles.text}>DISCOVER ARTISTS {'\n'}BEHIND THE {'\n'}CAMERA! </Text>
            <TouchableOpacity onPress= { () => navigation.navigate('StudioPreviewScreen')}>
                <View style={styles.press}>
                <Text style={styles.pressButton}> {'[ ◉¯]'} </Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: '#FAF3DD',
        justifyContent: 'center', 
        alignItems: 'center',
    },
    text: {
        fontSize: 33,
        marginTop: 20,
        marginLeft: 20,
        fontWeight: 'bold',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'left',
        color: '#1e3d59'
    },
    press: {
        width: 60,
        height: 60,
        marginLeft: 230,
        marginBottom: 65,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffc13b'
    },
    pressButton: {
        color: 'white',
        fontWeight: 'bold',
        marginBottom: 5,
        fontSize: 20
    }
});

export default HomeScreen;