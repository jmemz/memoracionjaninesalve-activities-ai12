import React from "react";
import {View, TouchableOpacity, Text, StyleSheet, Dimensions, ScrollView} from 'react-native'


const AFirstPackage = ({navigation}) => {
    return (
        <View style = {styles.container}>
        <ScrollView> 
            <Text style = {styles.text}>PACKAGE A </Text>
            <Text style = {styles.price}>PHP 60,000</Text>
                <View style = {styles.box}> 
                    <View style = {styles.label}> 
                        <Text style = {{textAlign: 'center', fontSize: 20, color: '#1e3d59' }}>Photo</Text>
                    </View>
                <Text style = {styles.inclusions}> PreNup Shoot + (Slideshow to be shown at the event){'\n\n'}Unlimited Event Photo Coverage
                    SDE(Same day Edit) Event Coverage {'\n\n'} 100pcs 4r size printed photos</Text>
                </View>
                <View style = {styles.box}> 
                    <View style = {styles.label}> 
                        <Text style = {{textAlign: 'center', fontSize: 20, color: '#1e3d59' }}>Video</Text>
                    </View>
                <Text style = {styles.inclusions}> PreNup Cinematic Video{'\n\n'}Event Highlight documentation{'\n\n'}Same Day Edit{'\n\n'}Drone shoots{'\n\n'}-  All copies will be stored at a free 32gb flashdrive  -</Text>
                </View>
            <TouchableOpacity onPress= { () => navigation.navigate('Home')}>
                <View style={styles.press}>
                <Text style={styles.pressButton}> {'Home'} </Text>
                </View>
            </TouchableOpacity>
        </ScrollView>
        </View>
    )
}
const {width} = Dimensions.get("screen");
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: '#FAF3DD',
        justifyContent: 'center', 
        alignItems: 'center',
    },
    text: {
        fontSize: 33,
        marginTop: 40,
        marginLeft: 10,
        fontWeight: 'bold',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        color: '#ff6e40'
    },
    press: {
        width: 120,
        height: 40,
        marginBottom: 20,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffc13b',
        marginLeft: 95,
        marginTop: 10,
    },
    pressButton: {
        color: 'white',
        fontWeight: 'bold',
        marginBottom: 5,
        fontSize: 20,
    },
    price: {
        fontSize: 25,
        fontWeight: 'bold',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        color: '#1e3d59',
        marginTop: 10,
        marginBottom: 15, 
    },
    inclusions:{
        marginLeft: 30,
        marginRight: 30,
        fontSize: 17,
        textAlign: 'center',
        color: 'white',
    },
    box: {
        backgroundColor: '#2D618F',
        width: width/1.15,
        padding: 15,
        borderRadius: 20,
        alignItems: 'center',
        marginBottom: 10,
    },
    label: {
        backgroundColor: 'white',
        width: width/1.5,
        borderRadius: 20,
        marginBottom: 12,
        height: 30,
    },
});

export default AFirstPackage;