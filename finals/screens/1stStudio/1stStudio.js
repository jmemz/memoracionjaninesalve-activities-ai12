import React from "react";
import {View, TouchableOpacity, Text, StyleSheet, Image, ScrollView, Dimensions } from 'react-native'


const FirstStudio = ({navigation}) => {
    return (
    <View style = {styles.container}> 
    <ScrollView>
        <View style= {styles.topTexts}>
        <Text style= {{top: 20, color: '#1e3d59', fontWeight: 'bold', right: 0, fontSize: 18}}>   MEMO CLIPS                      {'\n'}   Palo, Leyte  </Text>
            <Image source={require ('./images/mlogo.jpg')}
             style={{width:80,height:80, borderRadius: 50, top: 5, right: 10}} />
            <Text>  </Text>
        </View>
        <View style= {styles.topTexts}>
            <View style = {styles.column}>
                <Image source={require ('./images/memo4.jpg')}
                    style={{width:140,height:85, borderRadius: 10, top: 30, left:5}} />
                <Image source={require ('./images/memo1.jpg')}
                    style={{width:140,height:85, borderRadius: 10, top: 35, left:5}} />
                <Image source={require ('./images/memo3.jpg')}
                    style={{width:140,height:85, borderRadius: 10, top: 40, left:5}} />
            </View>
            <Image source={require ('./images/memo2.jpg')}
             style={{width:160,height:265, borderRadius: 10, top: 30, left: 10}} />
        </View>
        <Text style= {{top: 80, color: '#1e3d59', fontWeight: 'bold', textAlign: 'center', fontSize: 25, marginBottom: 30,}}>   PACKAGES  </Text>
        <View>
            <TouchableOpacity onPress= { () => navigation.navigate('A1')}> 
                 <View style={styles.press1}>
                <Text style={styles.pressButton}> PACKAGE   A </Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress= { () => navigation.navigate('A2')}> 
                 <View style={styles.press}>
                <Text style={styles.pressButton}> PACKAGE   B </Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress= { () => navigation.navigate('A3')}> 
                 <View style={styles.press}>
                <Text style={styles.pressButton}> PACKAGE   C </Text>
                </View>
            </TouchableOpacity>
        </View>
    </ScrollView>
    </View>
    )
}
const {width, height} = Dimensions.get("screen");
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: '#FAF3DD',
        justifyContent: 'center', 
        alignItems: 'center',
    },
    text: {
        fontSize: 40,
        fontWeight: 'bold',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        color: '#3d5a80'
    },
    press1: {
        width: 280,
        height: 40 ,
        marginTop: 65,
        marginLeft: 18,
        marginBottom: 10,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ff6e40'
    },
    press: {
        width: 280,
        height: 40 ,
        marginLeft: 18,
        marginBottom: 10,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ff6e40'
    },
    pressButton: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 19,
    },
    topTexts: {
        top: 30,
        flexDirection: 'row',
        alignItems: 'flex-start',
   },
    column: {
        flexDirection: 'column',
        alignItems: 'flex-start',
    }
});
export default FirstStudio;