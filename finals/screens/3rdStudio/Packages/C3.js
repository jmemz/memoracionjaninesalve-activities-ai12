import React from "react";
import {View, TouchableOpacity, Text, StyleSheet, Dimensions, ScrollView} from 'react-native'


const CThirdPackage = ({navigation}) => {
    return (
        <View style = {styles.container}>
        <ScrollView> 
            <Text style = {styles.text}>PACKAGE C </Text>
            <Text style = {styles.price}>PHP 45,000</Text>
                <View style = {styles.box}> 
                    <View style = {styles.label}> 
                        <Text style = {{textAlign: 'center', fontSize: 20, color: '#1e3d59' }}>Video</Text>
                    </View>
                <Text style = {styles.inclusions}>Pre Nuptial Teaser{'\n\n'}On the day Video Coverage
                {'\n\n'}Preparation to Reception Highlights{'\n\n'} Drone Footage {'\n\n'}
                Free raw files {'\n\n'}Free 32gb Flashdrive {'\n\n'}Same Day Edit Video</Text>
                </View>
            <TouchableOpacity onPress= { () => navigation.navigate('Home')}>
                <View style={styles.press}>
                <Text style={styles.pressButton}> {'Home'} </Text>
                </View>
            </TouchableOpacity>
        </ScrollView>
        </View>
    )
}
const {width} = Dimensions.get("screen");
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: '#FAF3DD',
        justifyContent: 'center', 
        alignItems: 'center',
    },
    text: {
        fontSize: 33,
        marginTop: 70,
        marginLeft: 10,
        fontWeight: 'bold',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        color: '#ff6e40'
    },
    press: {
        width: 120,
        height: 40,
        marginBottom: 20,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffc13b',
        marginLeft: 95,
        marginTop: 10,
    },
    pressButton: {
        color: 'white',
        fontWeight: 'bold',
        marginBottom: 5,
        fontSize: 20,
    },
    price: {
        fontSize: 25,
        fontWeight: 'bold',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        color: '#1e3d59',
        marginTop: 10,
        marginBottom: 15, 
    },
    inclusions:{
        marginLeft: 30,
        marginRight: 30,
        fontSize: 17,
        textAlign: 'center',
        color: 'white',
    },
    box: {
        backgroundColor: '#2D618F',
        width: width/1.15,
        padding: 15,
        borderRadius: 20,
        alignItems: 'center',
        marginBottom: 10,
    },
    label: {
        backgroundColor: 'white',
        width: width/1.5,
        borderRadius: 20,
        marginBottom: 12,
        height: 30,
    },
});

export default CThirdPackage;